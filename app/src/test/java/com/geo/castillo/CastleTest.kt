package com.geo.castillo

import com.geo.castillo.model.Castle
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class CastleTest {

    @Test
    fun testWindowsAsStringArray() {
        val array = Castle.getWindowsAsStringArray()
        assertEquals(" 1 - Left: Closed, Right: Closed", array.first())
    }

    @Test
    fun testWinners() {

        val arrayW = Castle.windows
        arrayW[1].rightWing.open()
        arrayW[1].leftWing.open()

        assertEquals(1, Castle.getWinners(false).count())

        arrayW.first().rightWing.open()
        arrayW.first().leftWing.open()

        assertEquals(2, Castle.getWinners(true).count())

        assertTrue(Castle.getWinners(true).contentEquals(arrayOf(1,2)))
    }
}