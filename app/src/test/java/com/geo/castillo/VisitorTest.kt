package com.geo.castillo

import com.geo.castillo.model.Window
import com.geo.castillo.model.visitor.VisitorCreator
import junit.framework.Assert.assertFalse
import junit.framework.TestCase.assertTrue
import org.junit.Assert.assertEquals
import org.junit.Test

class VisitorTest {

    @Test
    fun testFirstVisit() {
        val windows = Array(Constants.WINDOWS_COUNT) { Window(it) }
        val visitor = VisitorCreator.create(1)

        assertFalse(windows.first().leftWing.open)

        visitor.visit(windows)

        assertTrue(windows.first().leftWing.open)
        assertTrue(windows.last().leftWing.open)
        assertTrue(windows[30].leftWing.open)
    }

    @Test
    fun testOddVisit() {
        val windows = Array(Constants.WINDOWS_COUNT) { Window(it) }
        val visitor = VisitorCreator.create(3)

        assertFalse(windows.first().leftWing.open)
        windows[59].rightWing.open()
        visitor.visit(windows)

        assertFalse(windows.first().leftWing.open)
        assertFalse(windows.last().leftWing.open)
        assertTrue(windows[62].leftWing.open)
        assertTrue(windows[59].leftWing.open)
        assertFalse(windows[59].rightWing.open)
    }
}