package com.geo.castillo

import com.geo.castillo.model.visitor.First
import com.geo.castillo.model.visitor.Last
import com.geo.castillo.model.visitor.Odd
import com.geo.castillo.model.visitor.VisitorCreator
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class VisitorCreatorTest {

    @Test
    fun testCreator() {
        val visitor = VisitorCreator.create(1)
        Assert.assertTrue(visitor is First)
        Assert.assertEquals(1, visitor.number)

        val visitorOdd = VisitorCreator.create(3)
        Assert.assertTrue(visitorOdd is Odd)
        Assert.assertEquals(3, visitorOdd.number)

        val visitorLast = VisitorCreator.create(64)
        Assert.assertTrue(visitorLast is Last)
        Assert.assertEquals(64, visitorLast.number)
    }

    @Test
    fun testCreateAllVisitors() {
        assertEquals(64, VisitorCreator.createAll(64).count())

        assertTrue(VisitorCreator.createAll(64).first() is First)
    }
}