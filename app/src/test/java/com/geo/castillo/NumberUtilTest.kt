package com.geo.castillo

import com.geo.castillo.model.visitor.*
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class NumberUtilTest {

    @Test
    fun testLastMultiple() {
        assertEquals(63, 64.lastMultipleOf(3))
        assertEquals(60, 62.lastMultipleOf(3))
    }

    @Test
    fun testIsOddOrPair() {
        assertTrue( 64.isPair())
        assertTrue( 61.isOdd())
    }

}