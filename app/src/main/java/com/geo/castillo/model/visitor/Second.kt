package com.geo.castillo.model.visitor

import com.geo.castillo.model.Window

class Second(number: Int): Visitor(number) {
    override fun visit(windows: Array<Window>) {
        for (i in 0 until windows.size - 1 step 2) {
            val window = windows[i]
            window.openRight()
        }
    }
}