package com.geo.castillo.model

import android.util.Log
import com.geo.castillo.Constants.Companion.WINDOWS_COUNT
import com.geo.castillo.model.visitor.VisitorCreator

object Castle {

   var windows = Array(WINDOWS_COUNT) { Window(it + 1) }

   private val visitors = VisitorCreator.createAll(WINDOWS_COUNT)

    fun visit() {
        for (visitor in visitors) {
            visitor.visit(windows)
            Log.i("Castle", " ${visitor.number}")
        }
    }

    fun reset() {
        windows.forEach {
            it.leftWing.close()
            it.rightWing.close()
        }
    }

    fun getWindowsAsStringArray(): Array<String> = windows.map { it.toString() }.toTypedArray()

    private fun rule1(ant: Window, current: Window, next: Window) =
        ant.getState() == WindowState.C && current.getState() == WindowState.A
                && next.getState() == WindowState.C

    private fun rule2(current: Window) = current.getState() == WindowState.A


    fun getWinners(secondRuleActive: Boolean): Array<Int> =
        windows.filter {
            var isWinner = false
            if (it.number in 2..62) {
                val ant = windows[it.number - 2]
                val next = windows[it.number]
                isWinner = rule1(ant, it, next)
            }

            if (secondRuleActive) {
                isWinner = isWinner || rule2(it)
            }
            isWinner
        }.map { it.number }.toTypedArray()

    fun getWinnersAsArray(secondRuleActive: Boolean): Array<String> =
        getWinners(secondRuleActive).map { "$it" }.toTypedArray()

    fun getCountBy(state: WindowState): Int = windows.filter { it.getState() == state }.count()

}