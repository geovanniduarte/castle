package com.geo.castillo.model

import com.geo.castillo.model.Wing

enum class WindowState {
    A, C, I, D, U
}

class Window(val number: Int) {
    val leftWing = Wing()
    val rightWing = Wing()

    fun openLeft() {
        leftWing.open()
    }

    fun openRight() {
        rightWing.open()
    }

    fun openLeftWhenClosed() {
        leftWing.openWhenClosed()
    }

    fun openRightWhenClosed() {
        rightWing.openWhenClosed()
    }

    fun closeLeftWhenOpened() {
        leftWing.closeWhenOpened()
    }

    fun closeRightWhenOpened() {
        rightWing.closeWhenOpened()
    }

    fun toggleRight() {
        rightWing.toggle()
    }

    fun getState() = when {
        leftWing.open && rightWing.open -> WindowState.A
        !leftWing.open && !rightWing.open -> WindowState.C
        leftWing.open && !rightWing.open -> WindowState.I
        !leftWing.open && rightWing.open -> WindowState.D
        else -> WindowState.U
    }

    override fun toString(): String = " ${number} - Left: ${if (leftWing.open) "Opened" else "Closed" }, Right: ${if (rightWing.open) "Opened" else "Closed"} = ${getState().name}"
}