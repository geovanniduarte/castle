package com.geo.castillo.model.visitor

import com.geo.castillo.model.Window

class First(number: Int) : Visitor(number) {
    override fun visit(windows: Array<Window>) {
        for (window in windows) {
            window.openLeft()
        }
    }
}