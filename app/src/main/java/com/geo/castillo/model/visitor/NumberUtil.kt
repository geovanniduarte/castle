package com.geo.castillo.model.visitor

fun Int.lastMultipleOf(value: Int): Int {
    for (i in this downTo 0) {
       if (i % value == 0)
           return i
    }
    return value
}

fun Int.isPair() = this % 2 == 0

fun Int.isOdd() = !this.isPair()