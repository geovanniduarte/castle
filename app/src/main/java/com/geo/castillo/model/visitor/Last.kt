package com.geo.castillo.model.visitor

import com.geo.castillo.model.Window

class Last(number: Int) : Visitor(number) {
    override fun visit(windows: Array<Window>) {
        val window = windows.last()
        window.toggleRight()
    }
}