package com.geo.castillo.model.visitor

import com.geo.castillo.Constants
import java.lang.IllegalArgumentException

object VisitorCreator {

    fun createAll(count: Int): Array<Visitor> = Array(count) {
        create(it + 1)
    }

    fun create(number: Int): Visitor = when {
        number  == 1 -> First(number)
        number  == 2 -> Second(number)
        number == Constants.WINDOWS_COUNT -> Last(number)
        number.isPair() -> Pair(number)
        !number.isPair() -> Odd(number)
        else -> throw IllegalArgumentException("invalid number visitor")
    }
}
