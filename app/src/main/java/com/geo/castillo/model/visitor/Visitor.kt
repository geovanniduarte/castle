package com.geo.castillo.model.visitor

import com.geo.castillo.model.Window

abstract class Visitor(val number: Int) {
    abstract fun visit(windows: Array<Window>)
}