package com.geo.castillo.model

class Wing {
    var open = false

    fun open() {
        open = true
    }

    fun close() {
        open = false
    }

    fun openWhenClosed() {
        if (!open) {
            open = true
        }
    }

    fun closeWhenOpened() {
        if(open) {
            open = false
        }
    }

    //Open when closed and close when opened.
    fun toggle() {
        open = !open
    }
}