package com.geo.castillo.ui

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.geo.castillo.R

import kotlinx.android.synthetic.main.fragment_window.view.*


class MyWindowRecyclerViewAdapter(
    private var mValues: Array<String>
) : RecyclerView.Adapter<MyWindowRecyclerViewAdapter.ViewHolder>() {

    fun submitList(array: Array<String>) {
        mValues = array
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_window, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(private val mView: View) : RecyclerView.ViewHolder(mView) {
        fun bind(str: String) = with(itemView) {
            window_str.text = str
        }
    }
}
