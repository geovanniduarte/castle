package com.geo.castillo.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.geo.castillo.R
import com.geo.castillo.model.Castle
import com.geo.castillo.model.WindowState
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), WindowFragment.OnListFragmentInteractionListener {

    lateinit var allWindowsStateFragment: WindowFragment
    lateinit var winnersFragment: WindowFragment
    var isRuleChecked = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpAllWindowsList()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState?.run {
            putBoolean(CHECKED, isRuleChecked)
        }
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        savedInstanceState?.run {
            isRuleChecked = getBoolean(CHECKED, false)
        }
        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        updateLists()
        updateCounters()
    }

    private fun setUpAllWindowsList() {
        allWindowsStateFragment = WindowFragment.newInstance(1)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.content, allWindowsStateFragment, "windowlist")
            .addToBackStack(null)
            .commit()

        winnersFragment = WindowFragment.newInstance(4)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.winners_content, winnersFragment, "winnerslist")
            .addToBackStack(null)
            .commit()

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val rule2 = menu?.findItem(R.id.menu_rule_2)
        rule2?.isChecked = isRuleChecked
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when(item.itemId) {
        R.id.menu_visit -> {
            passVisitors()
            true
        }
        R.id.menu_reset -> {
            resetWindows()
            true
        }
        R.id.menu_rule_2 -> {
            item.isChecked = !item.isChecked
            isRuleChecked = item.isChecked
            false
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun updateLists() {
        allWindowsStateFragment.updateList(Castle.getWindowsAsStringArray())
        winnersFragment.updateList(Castle.getWinnersAsArray(isRuleChecked))
    }

    private fun updateCounters()  {
        txt_a_count.text = "${Castle.getCountBy(WindowState.A)}"
        txt_c_count.text = "${Castle.getCountBy(WindowState.C)}"
        txt_i_count.text = "${Castle.getCountBy(WindowState.I)}"
        txt_d_count.text = "${Castle.getCountBy(WindowState.D)}"
    }

    private fun passVisitors() {
        Castle.visit()
        updateLists()
        updateCounters()
    }

    private fun resetWindows() {
        Castle.reset()
        updateLists()
        updateCounters()
    }

    override fun onListFragmentInteraction(item: String?) {
       Toast.makeText(this, " $item ${getString(R.string.pressed)}", Toast.LENGTH_LONG).show()
    }

    companion object {
        val CHECKED = "rule_2_checked"
    }
}
